#![no_std]
#![cfg_attr(not(test), no_main)]

#[cfg_attr(not(test), panic_handler)]
fn my_panic(_info: &core::panic::PanicInfo) -> ! {
    loop {}
}

#[test]
fn demo() {
    panic!("hello from binary unit test")
}

#[cfg(not(test))]
#[no_mangle]
pub extern "C" fn _start() -> ! {
    // This is the normal entrypoint.
    loop {}
}
