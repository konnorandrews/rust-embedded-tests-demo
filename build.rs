fn main() {
    if std::env::var("CARGO_FEATURE_TESTING").is_err() {
        // Remove the need for libc.
        println!("cargo:rustc-link-arg-bins=-nostartfiles");
    }
}
